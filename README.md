## Health Daemon
Access to your health; Health analyses; freedom and the power of code applied to your health

## Prequesties 
install Node.js;https://nodejs.org/en/download/
install MongoDB; https://docs.mongodb.com/v3.4/tutorial/install-mongodb-on-windows/
install python:
custom: dbPath: C:\Users\<USER>\Dropbox\development\healthmon\data
logPath: C:\Users\<USER>\Dropbox\development\healthmon\log
pip install pymongo

## Usage
 * in bash: py ./main.py and answer questions
 * run ./run.sh and open the browser at https://localhost:3000

 [also]
 * from app folder run npm start
 * from server folder run node index.js
 * open http://localhost:4200 in the browser
 * run py main.py to add a new record

[production]
 * run ./build.sh
 * run ./reset.sh
 * run ./run.sh / nodemon server/

* Don't forget to chmod +x the .sh files.

## Functional design
Initially was thought of a python cli interface to retrieve data from user, or in the form of a simple GUI. The user would be able to filter down data and set ranges for the data in a web app. Also a graphical representation of the data would be displayed. Suggestions for actions to take to improve health will be listed.

Step 2 would be; integrating a correlation system. Auto suggestion options in the form of a dropdown will be estimated and added to the interface. Basically it is a machine learning project.

Behind the scenes the chatbot will also adapt to the users behavior. So the input is taken in account, but the user can also correct manually to get better results.


### Technical Thinking
* https://docs.python.org/2/library/tkinter.html
* pip3 install PyQt5
* https://python-gtk-3-tutorial.readthedocs.io/en/latest/
* https://www.gtk.org/download/windows.php
