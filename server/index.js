const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 3000;
const mongoose = require('mongoose');
const formidable = require('formidable');
const Schema = mongoose.Schema;
const http = require('http');
const bodyParser = require('body-parser')
const app = express();
const moment = require('moment');

app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});

mongoose.connect('mongodb://localhost/health', {
    useNewUrlParser: true
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Connected to the database..")
});

var healthRecordSchema = new mongoose.Schema({
    person: String,
    date: Date,
    sleep: String,
    activity: String,
    medication: String
});

var healthRecords = mongoose.model('health_records', healthRecordSchema);

app.get('/health-records', function (req, res){
        healthRecords.find({
        date: {
                $gte: moment(new Date(req.query.begin)).toISOString(),
                $lte: moment(new Date(req.query.end)).toISOString()
            }
        }, function(err, records) {
            if (err) {
                return console.error(err);
            }
            res.json(records);
        })
    
       console.log("GET REQUEST:", req.route.path, req.query.begin, req.query.end);

    });

app.use('/', express.static(path.join(__dirname, 'dist')))
 
// app.all('*', (req, res) => { 
//     res.status(200).sendFile(path.join(__dirname, 'dist')+ "/index.html");
// });

app.listen(PORT, () => console.log(`Listening on ${ PORT }`));
