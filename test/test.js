var cl = console.log;

function filterBy(type, keyword, data) {
    return data.filter(function(d) {
        return d[type].includes(keyword);
    }) 
}

cl(filterBy('sleep', '9:00AM', [{a:0, sleep: "9:00AM"}, {a:1, sleep: "9:00PM"}]));

var date1 = "9:00AM - 8:00AM";
var date2 = "10:00PM - 7:00AM";
var date3 = "10:15PM - 7:30AM";

function getMinutes(date){
    return parseInt(date.slice(date.indexOf(":")+1, date.indexOf("M")-1));
}

function diff() {

}

cl(getMinutes("9:35AM"));

function convertMinutes(a, b) {
    return (a + b) / 60;
}

function convD(date) {
    var begin = date.slice(0, date.indexOf("-")-1);
    var end = date.slice(date.indexOf("-")+2, date.length);
    var evening = parseInt(begin);
    var morning = parseInt(end);
    var minutesH1 = getMinutes(begin); // 15
    var minutesH2 = getMinutes(end); // 30
    var tillMidnight = 12 - evening; // till midnight
    var inclMorning = tillMidnight + morning;

    // difference between 10:15AM and 7:30AM
    // expect: 9:15
    cl(getMinutes(begin), getMinutes(end));

     function convertMinsToHrsMins(minutes) {
        var h = Math.floor(minutes / 60);
        var m = minutes % 60;
        h = h < 10 ? '0' + h : h;
        m = m < 10 ? '0' + m : m;
        return h + ':' + m;
      }

    cl('hours', convertMinsToHrsMins(75));
}

convD("10:15PM - 7:30AM");