(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/account.service.ts":
/*!************************************!*\
  !*** ./src/app/account.service.ts ***!
  \************************************/
/*! exports provided: AccountService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountService", function() { return AccountService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AccountService = /** @class */ (function () {
    function AccountService() {
        this.display = false;
    }
    AccountService.prototype.toggle = function () {
        console.log(1);
        this.display = !this.display;
    };
    AccountService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AccountService);
    return AccountService;
}());



/***/ }),

/***/ "./src/app/account/account.component.html":
/*!************************************************!*\
  !*** ./src/app/account/account.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"account\" [ngStyle]=\"{'width': _account.display ? '200px' : '0px'}\">\n  <div class=\"form-group\">\n    <input type=\"text\" class=\"form-control\" placeholder=\"Username\"/>\n    <input type=\"password\" class=\"form-control\" placeholder=\"Password\"/>\n    <button class=\"form-control btn btn-primary\" [routerLink]=\"healthrecords\">Login</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/account/account.component.scss":
/*!************************************************!*\
  !*** ./src/app/account/account.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".account {\n  position: fixed;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 999;\n  overflow: hidden;\n  z-index: 999;\n  width: 0px;\n  top: 50px;\n  left: 0;\n  bottom: 0;\n  transition: ease-in-out 0.2s; }\n  .account .form-group {\n    margin: 10px; }\n  .account .form-control {\n    margin-bottom: 10px;\n    font-size: 12px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWNjb3VudC9DOlxcVXNlcnNcXGhrc18xXFxEcm9wYm94XFxkZXZlbG9wbWVudFxcaGVhbHRoZGFlbW9uXFxhcHAvc3JjXFxhcHBcXGFjY291bnRcXGFjY291bnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFlO0VBQ2Ysb0NBQWlDO0VBQ2pDLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLFVBQVU7RUFDVixTQUFTO0VBQ1QsT0FBTztFQUNQLFNBQVM7RUFDVCw0QkFBNEIsRUFBQTtFQVZoQztJQWFRLFlBQVksRUFBQTtFQWJwQjtJQWlCUSxtQkFBbUI7SUFDbkIsZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvYWNjb3VudC9hY2NvdW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFjY291bnQge1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjUpO1xyXG4gICAgei1pbmRleDogOTk5O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHotaW5kZXg6IDk5OTtcclxuICAgIHdpZHRoOiAwcHg7XHJcbiAgICB0b3A6IDUwcHg7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgdHJhbnNpdGlvbjogZWFzZS1pbi1vdXQgMC4ycztcclxuXHJcbiAgICAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuZm9ybS1jb250cm9sIHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/account/account.component.ts":
/*!**********************************************!*\
  !*** ./src/app/account/account.component.ts ***!
  \**********************************************/
/*! exports provided: AccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountComponent", function() { return AccountComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../account.service */ "./src/app/account.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var AccountComponent = /** @class */ (function () {
    function AccountComponent(_account) {
        this._account = _account;
    }
    AccountComponent.prototype.ngOnInit = function () {
    };
    AccountComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-account',
            template: __webpack_require__(/*! ./account.component.html */ "./src/app/account/account.component.html"),
            styles: [__webpack_require__(/*! ./account.component.scss */ "./src/app/account/account.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_account_service__WEBPACK_IMPORTED_MODULE_1__["AccountService"]])
    ], AccountComponent);
    return AccountComponent;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<app-account></app-account>\r\n<app-leke-menu></app-leke-menu>\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Health Daemon';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./account.service */ "./src/app/account.service.ts");
/* harmony import */ var _data_formatter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./data-formatter.service */ "./src/app/data-formatter.service.ts");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./database.service */ "./src/app/database.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./list/list.component */ "./src/app/list/list.component.ts");
/* harmony import */ var _toolbar_toolbar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./toolbar/toolbar.component */ "./src/app/toolbar/toolbar.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _chart_chart_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./chart/chart.component */ "./src/app/chart/chart.component.ts");
/* harmony import */ var _views_health_records_health_records_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./views/health-records/health-records.component */ "./src/app/views/health-records/health-records.component.ts");
/* harmony import */ var _account_account_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./account/account.component */ "./src/app/account/account.component.ts");
/* harmony import */ var _views_entry_entry_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./views/entry/entry.component */ "./src/app/views/entry/entry.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _leke_menu_leke_menu_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./leke-menu/leke-menu.component */ "./src/app/leke-menu/leke-menu.component.ts");
/* harmony import */ var _menu_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./menu.service */ "./src/app/menu.service.ts");
/* harmony import */ var _views_account_overview_account_overview_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./views/account-overview/account-overview.component */ "./src/app/views/account-overview/account-overview.component.ts");























var routes = [
    { path: '', redirectTo: '/healthrecords', pathMatch: 'full' },
    { path: 'entry', component: _views_entry_entry_component__WEBPACK_IMPORTED_MODULE_17__["EntryComponent"] },
    { path: 'healthrecords', component: _views_health_records_health_records_component__WEBPACK_IMPORTED_MODULE_15__["HealthRecordsComponent"], runGuardsAndResolvers: 'always' },
    { path: 'healthrecords/:range', component: _views_health_records_health_records_component__WEBPACK_IMPORTED_MODULE_15__["HealthRecordsComponent"], runGuardsAndResolvers: 'always' },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _list_list_component__WEBPACK_IMPORTED_MODULE_8__["ListComponent"],
                _toolbar_toolbar_component__WEBPACK_IMPORTED_MODULE_9__["ToolbarComponent"],
                _chart_chart_component__WEBPACK_IMPORTED_MODULE_14__["ChartComponent"],
                _views_health_records_health_records_component__WEBPACK_IMPORTED_MODULE_15__["HealthRecordsComponent"],
                _account_account_component__WEBPACK_IMPORTED_MODULE_16__["AccountComponent"],
                _views_entry_entry_component__WEBPACK_IMPORTED_MODULE_17__["EntryComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_18__["HeaderComponent"],
                _leke_menu_leke_menu_component__WEBPACK_IMPORTED_MODULE_19__["LekeMenuComponent"],
                _views_account_overview_account_overview_component__WEBPACK_IMPORTED_MODULE_21__["AccountOverviewComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forRoot(routes, { enableTracing: false }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatDatepickerModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_10__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatFormFieldModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"],
            ],
            providers: [_data_formatter_service__WEBPACK_IMPORTED_MODULE_2__["DataFormatterService"], _database_service__WEBPACK_IMPORTED_MODULE_3__["DatabaseService"], _menu_service__WEBPACK_IMPORTED_MODULE_20__["MenuService"], _account_service__WEBPACK_IMPORTED_MODULE_1__["AccountService"], _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatDatepickerModule"], Document],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/chart/chart.component.html":
/*!********************************************!*\
  !*** ./src/app/chart/chart.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid charts\">\r\n    <div class=\"row\">\r\n        <div class=\"card chart-control\">\r\n            <h5 class=\"card-title\">View settings</h5>\r\n            <div class=\"card-body \">\r\n                <div class=\"form-check\">\r\n                    <input type=\"checkbox\" class=\"form-check-input\" id=\"scrollSim\">\r\n                    <label class=\"form-check-label\" for=\"scrollSim\">Scroll simultaneously</label>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"chart\" *ngFor=\"let data of getData()\">\r\n            <div class=\"text\">{{data.text}}</div>\r\n            <div class=\"bar-wrap\" *ngFor=\"let value of data.values\">\r\n                <div class=\"bar\" [ngStyle]=\"{\r\n                    'height': (value / 24 *100) + '%',\r\n                    'background-color': _dataFormatter.getColor(data.text, value)}\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/chart/chart.component.scss":
/*!********************************************!*\
  !*** ./src/app/chart/chart.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".chart-control {\n  margin: 0px 10px;\n  padding: 10px; }\n  .chart-control .card-body {\n    padding: 0px; }\n  .chart-control h5 {\n    font-size: 12px; }\n  .charts {\n  font-size: 12px; }\n  .chart {\n  display: block;\n  width: 100%;\n  height: 120px;\n  border: 1px solid #efefef;\n  overflow-y: hidden;\n  overflow-x: scroll;\n  white-space: nowrap;\n  margin: 10px;\n  padding-bottom: 18px; }\n  .chart .text {\n    font-size: 12px;\n    background-color: #efefef; }\n  .bar-wrap {\n  display: inline-block;\n  position: relative;\n  height: 100%;\n  width: 4px; }\n  .bar-wrap .bar {\n    position: absolute;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    background-color: #dee2e6; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhcnQvQzpcXFVzZXJzXFxoa3NfMVxcRHJvcGJveFxcZGV2ZWxvcG1lbnRcXGhlYWx0aGRhZW1vblxcYXBwL3NyY1xcYXBwXFxjaGFydFxcY2hhcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBZ0I7RUFDaEIsYUFBYSxFQUFBO0VBRmpCO0lBS1EsWUFBWSxFQUFBO0VBTHBCO0lBU1EsZUFBZSxFQUFBO0VBSXZCO0VBQ0ksZUFBZSxFQUFBO0VBR25CO0VBQ0ksY0FBYztFQUNkLFdBQVc7RUFDWCxhQUFhO0VBQ2IseUJBQXlCO0VBQ3pCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixvQkFBb0IsRUFBQTtFQVR4QjtJQVlRLGVBQWU7SUFDZix5QkFBeUIsRUFBQTtFQUdqQztFQUNJLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFVBQVUsRUFBQTtFQUpkO0lBT1Esa0JBQWtCO0lBQ2xCLE9BQU87SUFDUCxRQUFRO0lBQ1IsU0FBUztJQUNULHlCQUNKLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jaGFydC9jaGFydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jaGFydC1jb250cm9sIHtcclxuICAgIG1hcmdpbjogMHB4IDEwcHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgXHJcbiAgICAuY2FyZC1ib2R5IHtcclxuICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgaDUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIH1cclxufVxyXG5cclxuLmNoYXJ0cyB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuXHJcbi5jaGFydCB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMjBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlZmVmZWY7XHJcbiAgICBvdmVyZmxvdy15OiBoaWRkZW47XHJcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDE4cHg7XHJcblxyXG4gICAgLnRleHQge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWZlZmVmO1xyXG4gICAgfVxyXG59XHJcbi5iYXItd3JhcCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogNHB4O1xyXG5cclxuICAgIC5iYXIge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGVlMmU2XHJcbiAgICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/chart/chart.component.ts":
/*!******************************************!*\
  !*** ./src/app/chart/chart.component.ts ***!
  \******************************************/
/*! exports provided: ChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartComponent", function() { return ChartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _data_formatter_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../data-formatter.service */ "./src/app/data-formatter.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../database.service */ "./src/app/database.service.ts");




var ChartComponent = /** @class */ (function () {
    function ChartComponent(_db, _dataFormatter) {
        this._db = _db;
        this._dataFormatter = _dataFormatter;
    }
    ChartComponent.prototype.getData = function () {
        return this._dataFormatter
            .getChartData(this._dataFormatter.applyFilters(this._dataFormatter.filters, this.data));
    };
    ChartComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ChartComponent.prototype, "data", void 0);
    ChartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-chart',
            template: __webpack_require__(/*! ./chart.component.html */ "./src/app/chart/chart.component.html"),
            styles: [__webpack_require__(/*! ./chart.component.scss */ "./src/app/chart/chart.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_database_service__WEBPACK_IMPORTED_MODULE_3__["DatabaseService"],
            _data_formatter_service__WEBPACK_IMPORTED_MODULE_1__["DataFormatterService"]])
    ], ChartComponent);
    return ChartComponent;
}());



/***/ }),

/***/ "./src/app/data-formatter.service.ts":
/*!*******************************************!*\
  !*** ./src/app/data-formatter.service.ts ***!
  \*******************************************/
/*! exports provided: DataFormatterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataFormatterService", function() { return DataFormatterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./database.service */ "./src/app/database.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var DataFormatterService = /** @class */ (function () {
    function DataFormatterService(_db, router) {
        this._db = _db;
        this.router = router;
        this.filter = {};
        this.filterType = "person";
        this.filters = [];
        this.range = {};
        this.filterTerms = '';
    }
    DataFormatterService.prototype.getSleepTimes = function (a) {
        var beginSleep = a.slice(0, a.indexOf("-") - 3);
        var endSleep = a.slice(a.indexOf("-") + 1, a.length - 2);
        var hoursA = beginSleep.slice(0, beginSleep.indexOf(":"));
        var minA = beginSleep.slice(beginSleep.indexOf(":") + 1, beginSleep.length);
        var hoursB = endSleep.slice(0, endSleep.indexOf(":"));
        var minB = endSleep.slice(endSleep.indexOf(":") + 1, endSleep.length);
        function minutesToHours(a) {
            var hours = Math.trunc(a / 60);
            var minutes = a % 60;
            return parseFloat(hours + "." + minutes);
        }
        var total = minutesToHours(((12 - parseInt(hoursA) + parseInt(hoursB)) * 60) + parseInt(minA) + parseInt(minB));
        function toFloat(value) {
            return parseFloat(value.replace(":", "."));
        }
        beginSleep = toFloat(beginSleep);
        endSleep = toFloat(endSleep);
        return { beginSleep: beginSleep, endSleep: endSleep, total: total };
    };
    DataFormatterService.prototype.formatDate = function (date) {
        return moment__WEBPACK_IMPORTED_MODULE_3__(date.slice(0, 10)).format("YYYY-MM-DD");
    };
    DataFormatterService.prototype.getColor = function (factor, sleep) {
        var color;
        var totalSleep = sleep; // .. temporarily directly put in total sleep time
        switch (factor) {
            case 'Total Sleep':
                // console.log(totalSleep)
                if (totalSleep < 6) { // sleep formatting needed to get value
                    color = "rgba(225, 0, 0, " + (totalSleep / 10) + ")";
                }
                else {
                    color = "rgba(0, 255, 0, " + (totalSleep / 10) + ")";
                }
                break;
            default:
                color = "#dee2e6";
        }
        return color;
    };
    DataFormatterService.prototype.getChartData = function (data) {
        var _this = this;
        return [
            {
                text: "Total Sleep",
                values: data.map(function (d) {
                    return _this.getSleepTimes(d.sleep).total;
                })
            },
            {
                text: "Begin Sleep",
                values: data.map(function (d) {
                    return _this.getSleepTimes(d.sleep).beginSleep;
                })
            },
            {
                text: "End Sleep",
                values: data.map(function (d) {
                    return _this.getSleepTimes(d.sleep).endSleep;
                })
            }
        ];
    };
    DataFormatterService.prototype.setFilterType = function (type) {
        this.filterType = type;
        this.filter['prop'] = type;
        console.log("[TYPE]:", type);
    };
    DataFormatterService.prototype.setFilterTerms = function (terms) {
        this.filterTerms = terms;
        this.filter['value'] = terms;
    };
    DataFormatterService.prototype.addFilter = function (filter) {
        this.filters.push(filter);
        this.filter = {};
        this.filterTerms = ''; //
    };
    DataFormatterService.prototype.removeFilter = function (filter) {
        this.filters = this.filters.filter(function (f) {
            return f !== filter;
        });
    };
    DataFormatterService.prototype.getFilters = function () {
        return this.filters;
    };
    DataFormatterService.prototype.applyFilters = function (filters, records) {
        var _this = this;
        filters.forEach(function (filter) {
            records = records.filter(function (record) { return record[filter.prop].includes(filter.value); });
        });
        //
        console.log(this.filterType, this.filterTerms);
        records = records.filter(function (record) {
            return record[_this.filterType].includes(_this.filterTerms);
        });
        return records;
    };
    DataFormatterService.prototype.setSearchQuery = function (terms) {
        this.filterTerms = terms;
    };
    DataFormatterService.prototype.getRecords = function () {
        return this.records;
    };
    DataFormatterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], DataFormatterService);
    return DataFormatterService;
}());



/***/ }),

/***/ "./src/app/database.service.ts":
/*!*************************************!*\
  !*** ./src/app/database.service.ts ***!
  \*************************************/
/*! exports provided: DatabaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatabaseService", function() { return DatabaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);




var DatabaseService = /** @class */ (function () {
    function DatabaseService(http) {
        this.http = http;
    }
    DatabaseService.prototype.getRecords = function (range) {
        var begin = moment__WEBPACK_IMPORTED_MODULE_3__(Date.now()).subtract(1, 'months').format("YYYY/MM/DD");
        var end = moment__WEBPACK_IMPORTED_MODULE_3__(Date.now()).format("YYYY/MM/DD");
        if (range) {
            begin = moment__WEBPACK_IMPORTED_MODULE_3__(range.begin).format("YYYY-MM-DD");
            end = moment__WEBPACK_IMPORTED_MODULE_3__(range.end).format("YYYY-MM-DD");
        }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('begin', begin)
            .set('end', end);
        return this.http.get("http://localhost:3000/health-records", { params: params });
    };
    DatabaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], DatabaseService);
    return DatabaseService;
}());



/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header\">\r\n  <div class=\"btn btn-default\">\r\n    <a (click)=\"_menu.action('toggle', 0)\">\r\n      {{_menu.opened.length > 0 ? '⊗' : '≡' }}\r\n    </a>\r\n  </div>\r\n  <div class=\"btn btn-default\">\r\n    <a (click)=\"_menu.toggleOrientation()\">\r\n      {{_menu.orientation === 'top' ? '◄' : '▲'}}\r\n    </a>\r\n  </div>\r\n  <div class=\"btn btn-default\">\r\n    <a (click)=\"_account.toggle()\">\r\n        {{_account.display === false ? '■' : '▲'}}\r\n    </a>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  height: 50px;\n  background-color: black; }\n  .header .btn {\n    float: left;\n    padding: 0;\n    color: #fff;\n    margin: 0;\n    font-size: 10px;\n    width: auto;\n    height: auto;\n    cursor: pointer;\n    border: none;\n    border-radius: 0px; }\n  .header .btn a {\n      padding: 15px 0px;\n      box-sizing: border-box;\n      height: 50px;\n      width: 50px;\n      display: block;\n      cursor: pointer;\n      transition: ease-in-out 0.2s; }\n  .header .btn a:hover {\n      background-color: rgba(255, 120, 255, 0.2); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhZGVyL0M6XFxVc2Vyc1xcaGtzXzFcXERyb3Bib3hcXGRldmVsb3BtZW50XFxoZWFsdGhkYWVtb25cXGFwcC9zcmNcXGFwcFxcaGVhZGVyXFxoZWFkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFlO0VBQ2YsTUFBTTtFQUNOLE9BQU87RUFDUCxRQUFRO0VBQ1IsWUFBWTtFQUNaLHVCQUErQixFQUFBO0VBTm5DO0lBU1EsV0FBVztJQUNYLFVBQVU7SUFDVixXQUFXO0lBQ1gsU0FBUztJQUNULGVBQWU7SUFDZixXQUFXO0lBQ1gsWUFBWTtJQUNaLGVBQWU7SUFDZixZQUFZO0lBQ1osa0JBQWtCLEVBQUE7RUFsQjFCO01BcUJZLGlCQUFpQjtNQUNqQixzQkFBc0I7TUFDdEIsWUFBWTtNQUNaLFdBQVc7TUFDWCxjQUFjO01BQ2QsZUFBZTtNQUNmLDRCQUE0QixFQUFBO0VBM0J4QztNQStCWSwwQ0FBd0MsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMSk7XHJcblxyXG4gICAgLmJ0biB7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgICAgIGhlaWdodDogYXV0bztcclxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuXHJcbiAgICAgICAgYSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDE1cHggMHB4O1xyXG4gICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBlYXNlLWluLW91dCAwLjJzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgYTpob3ZlciB7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LDEyMCwyNTUsIDAuMik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../account.service */ "./src/app/account.service.ts");
/* harmony import */ var _menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../menu.service */ "./src/app/menu.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");




var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(_menu, _account) {
        this._menu = _menu;
        this._account = _account;
    }
    HeaderComponent.prototype.toggle = function () {
        this._account.toggle();
    };
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"], _account_service__WEBPACK_IMPORTED_MODULE_1__["AccountService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/health-records.service.ts":
/*!*******************************************!*\
  !*** ./src/app/health-records.service.ts ***!
  \*******************************************/
/*! exports provided: HealthRecordsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HealthRecordsService", function() { return HealthRecordsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./database.service */ "./src/app/database.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var HealthRecordsService = /** @class */ (function () {
    function HealthRecordsService(_db) {
        this._db = _db;
    }
    HealthRecordsService.prototype.request = function (range) {
        this.$records = this._db.getRecords(range);
    };
    HealthRecordsService.prototype.get = function () {
        return this.$records;
    };
    HealthRecordsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_database_service__WEBPACK_IMPORTED_MODULE_1__["DatabaseService"]])
    ], HealthRecordsService);
    return HealthRecordsService;
}());



/***/ }),

/***/ "./src/app/leke-menu/leke-menu.component.html":
/*!****************************************************!*\
  !*** ./src/app/leke-menu/leke-menu.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"level\" [ngClass]=\"{'top': _menu.orientation === 'top', 'left': _menu.orientation === 'left'}\"*ngFor=\"let level of _menu.structure.levels\" \r\n    [ngStyle]=\"{'height': _menu.orientation === 'top' ? level.size : auto,\r\n                'width': _menu.orientation === 'left' ? level.size : auto}\"> {{orientation}}\r\n    <div class=\"text\">\r\n        <a class=\"level-link\" (click)=\"_menu.action('closeUntil', level.id)\">\r\n            {{level.text}}\r\n        </a>\r\n    </div>\r\n    <div class=\"wrapper\">\r\n        <div class=\"item\" *ngFor=\"let item of _menu.getList(level.listId, _menu.structure).items\">\r\n            <a class=\"item-link\" (click)=\"_menu.action(item.type, item.path)\">\r\n              {{item.text}}\r\n            </a> \r\n          </div> \r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/leke-menu/leke-menu.component.scss":
/*!****************************************************!*\
  !*** ./src/app/leke-menu/leke-menu.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".level {\n  position: fixed;\n  transition: 0.2s ease-in-out;\n  background-color: rgba(0, 0, 0, 0.5);\n  box-sizing: border-box;\n  z-index: 999; }\n  .level .text {\n    position: absolute;\n    font-size: 14px;\n    line-height: 50px;\n    color: #fff;\n    background-color: rgba(0, 0, 0, 0.5);\n    text-align: left;\n    margin: 0;\n    box-sizing: border-box; }\n  .level .level-link {\n    display: block;\n    padding: 10px;\n    font-size: 12px;\n    line-height: 12px;\n    height: 30px;\n    cursor: pointer;\n    transition: 0.2s ease-in-out; }\n  .level .item {\n    position: relative;\n    background-color: rgba(0, 0, 0, 0.5);\n    color: #fff;\n    float: left;\n    box-sizing: border-box; }\n  .level .item .item-link {\n      display: block;\n      padding: 10px;\n      font-size: 10px;\n      line-height: 10px;\n      cursor: pointer;\n      transition: 0.2s ease-in-out; }\n  .level .item .item-link:hover {\n      background-color: rgba(0, 0, 0, 0.5); }\n  .level.top {\n  left: 0;\n  top: 50px;\n  right: 0;\n  overflow-x: auto;\n  overflow-y: hidden; }\n  .level.top .text {\n    bottom: 0;\n    left: 0;\n    right: 0;\n    height: 30px; }\n  .level.top .item {\n    width: 150px;\n    height: 30px; }\n  .level.left {\n  left: 0;\n  top: 50px;\n  bottom: 0; }\n  .level.left .wrapper {\n    margin-right: 30px;\n    height: 100%;\n    overflow-x: auto; }\n  .level.left .text {\n    top: 0;\n    bottom: 0;\n    right: 0;\n    width: 30px; }\n  .level.left .text .level-link {\n      position: relative;\n      height: 100%;\n      -webkit-writing-mode: tb-rl;\n          -ms-writing-mode: tb-rl;\n              writing-mode: tb-rl; }\n  .level.left .item {\n    width: 100%;\n    height: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGVrZS1tZW51L0M6XFxVc2Vyc1xcaGtzXzFcXERyb3Bib3hcXGRldmVsb3BtZW50XFxoZWFsdGhkYWVtb25cXGFwcC9zcmNcXGFwcFxcbGVrZS1tZW51XFxsZWtlLW1lbnUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFlO0VBQ2YsNEJBQTRCO0VBQzVCLG9DQUFpQztFQUNqQyxzQkFBc0I7RUFDdEIsWUFBWSxFQUFBO0VBTGhCO0lBUVEsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsV0FBVztJQUNYLG9DQUFpQztJQUNqQyxnQkFBZ0I7SUFDaEIsU0FBUztJQUNULHNCQUFzQixFQUFBO0VBZjlCO0lBbUJRLGNBQWM7SUFDZCxhQUFhO0lBQ2IsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixZQUFZO0lBQ1osZUFBZTtJQUNmLDRCQUE0QixFQUFBO0VBekJwQztJQTZCUSxrQkFBa0I7SUFDbEIsb0NBQWlDO0lBQ2pDLFdBQVc7SUFDWCxXQUFXO0lBQ1gsc0JBQXNCLEVBQUE7RUFqQzlCO01Bb0NZLGNBQWM7TUFDZCxhQUFhO01BQ2IsZUFBZTtNQUNmLGlCQUFpQjtNQUNqQixlQUFlO01BQ2YsNEJBQTRCLEVBQUE7RUF6Q3hDO01BNENZLG9DQUFpQyxFQUFBO0VBSzdDO0VBQ0ksT0FBTztFQUNQLFNBQVM7RUFDVCxRQUFRO0VBQ1IsZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBO0VBTHRCO0lBUVEsU0FBUztJQUNULE9BQU87SUFDUCxRQUFRO0lBQ1IsWUFBWSxFQUFBO0VBWHBCO0lBZVEsWUFBWTtJQUNaLFlBQVksRUFBQTtFQUlwQjtFQUNJLE9BQU87RUFDUCxTQUFTO0VBQ1QsU0FBUyxFQUFBO0VBSGI7SUFNUSxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLGdCQUFnQixFQUFBO0VBUnhCO0lBWVEsTUFBTTtJQUNOLFNBQVM7SUFDVCxRQUFRO0lBQ1IsV0FBVyxFQUFBO0VBZm5CO01Ba0JZLGtCQUFrQjtNQUNsQixZQUFZO01BQ1osMkJBQW1CO1VBQW5CLHVCQUFtQjtjQUFuQixtQkFBbUIsRUFBQTtFQXBCL0I7SUF5QlEsV0FBVztJQUNYLFlBQVksRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xla2UtbWVudS9sZWtlLW1lbnUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGV2ZWwge1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgdHJhbnNpdGlvbjogMC4ycyBlYXNlLWluLW91dDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KTtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICB6LWluZGV4OiA5OTk7XHJcbiAgICBcclxuICAgIC50ZXh0IHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC41KTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgfVxyXG5cclxuICAgIC5sZXZlbC1saW5rIHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMTJweDtcclxuICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgIHRyYW5zaXRpb246IDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgICB9XHJcblxyXG4gICAgLml0ZW0ge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSk7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuXHJcbiAgICAgICAgLml0ZW0tbGluayB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxMHB4O1xyXG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICAgICAgICAgIHRyYW5zaXRpb246IDAuMnMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5pdGVtLWxpbms6aG92ZXIge1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4ubGV2ZWwudG9wIHtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0b3A6IDUwcHg7XHJcbiAgICByaWdodDogMDtcclxuICAgIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgICBvdmVyZmxvdy15OiBoaWRkZW47XHJcblxyXG4gICAgLnRleHQge1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgIGhlaWdodDogMzBweDtcclxuICAgIH1cclxuXHJcbiAgICAuaXRlbSB7XHJcbiAgICAgICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgICAgIGhlaWdodDogMzBweDtcclxuICAgIH1cclxufVxyXG5cclxuLmxldmVsLmxlZnQge1xyXG4gICAgbGVmdDogMDtcclxuICAgIHRvcDogNTBweDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIFxyXG4gICAgLndyYXBwZXIge1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMzBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgb3ZlcmZsb3cteDogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICAudGV4dCB7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICByaWdodDogMDtcclxuICAgICAgICB3aWR0aDogMzBweDtcclxuXHJcbiAgICAgICAgLmxldmVsLWxpbmsge1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgd3JpdGluZy1tb2RlOiB0Yi1ybDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLml0ZW0ge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMzBweDtcclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/leke-menu/leke-menu.component.ts":
/*!**************************************************!*\
  !*** ./src/app/leke-menu/leke-menu.component.ts ***!
  \**************************************************/
/*! exports provided: LekeMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LekeMenuComponent", function() { return LekeMenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _account_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../account.service */ "./src/app/account.service.ts");
/* harmony import */ var _menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../menu.service */ "./src/app/menu.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var LekeMenuComponent = /** @class */ (function () {
    function LekeMenuComponent(_menu, _router, _account, elm) {
        this._menu = _menu;
        this._router = _router;
        this._account = _account;
        this.elm = elm;
    }
    LekeMenuComponent.prototype.ngOnInit = function () {
    };
    LekeMenuComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var menu = this.elm.nativeElement.children[0];
        this.elm.nativeElement.parentElement.parentElement.addEventListener('click', function (event) {
            if (menu.clientWidth < event.clientX && menu.clientWidth !== 0)
                _this._menu.action('toggle', 0);
                _this._account.toggle();
        });
    };
    LekeMenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-leke-menu',
            template: __webpack_require__(/*! ./leke-menu.component.html */ "./src/app/leke-menu/leke-menu.component.html"),
            styles: [__webpack_require__(/*! ./leke-menu.component.scss */ "./src/app/leke-menu/leke-menu.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _account_service__WEBPACK_IMPORTED_MODULE_1__["AccountService"], _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]])
    ], LekeMenuComponent);
    return LekeMenuComponent;
}());



/***/ }),

/***/ "./src/app/list/list.component.html":
/*!******************************************!*\
  !*** ./src/app/list/list.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container health-records\">\r\n  <div class=\"row\">\r\n      <table class=\"table health-records-table\">\r\n          <thead>\r\n            <tr>\r\n              <th scope=\"col\">#</th>\r\n              <th scope=\"col\">Date ▼</th>\r\n              <th scope=\"col\">Person</th>\r\n              <th scope=\"col\">Sleep quantity</th>\r\n              <th scope=\"col\">Sleep quality</th>\r\n              <th scope=\"col\">Activity</th>\r\n              <th scope=\"col\">Medication</th>\r\n            </tr>\r\n          </thead>\r\n          <tbody>\r\n            <tr *ngFor=\"let record of getRecords(); let i = index\">\r\n              <th scope=\"row\">{{ i+1 }}</th>\r\n              <td>{{ _dataFormatter.formatDate(record.date) }}</td>\r\n              <td>{{ record.person }}</td>\r\n              <td>{{ record.sleep }}</td>\r\n              <td>{{ record.sleep_quality }}</td>\r\n              <td>{{ record.activity }}</td>\r\n              <td>{{ record.medication }}</td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n  </div>\r\n</div>\r\n\r\n{{ records }}\r\n"

/***/ }),

/***/ "./src/app/list/list.component.scss":
/*!******************************************!*\
  !*** ./src/app/list/list.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".health-records {\n  height: 400px;\n  overflow-y: scroll;\n  box-sizing: border-box; }\n\n.health-records-table {\n  font-size: 12px; }\n\n.health-records-table th {\n    padding: 0rem; }\n\n.health-records-table thead th {\n    border-bottom: 1px solid #dee2e6;\n    background-color: #dee2e6; }\n\n.health-records-table tbody td {\n    padding: 0rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGlzdC9DOlxcVXNlcnNcXGhrc18xXFxEcm9wYm94XFxkZXZlbG9wbWVudFxcaGVhbHRoZGFlbW9uXFxhcHAvc3JjXFxhcHBcXGxpc3RcXGxpc3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLGVBQWUsRUFBQTs7QUFEbkI7SUFJUSxhQUFhLEVBQUE7O0FBSnJCO0lBU1ksZ0NBQWdDO0lBQ2hDLHlCQUF5QixFQUFBOztBQVZyQztJQWVZLGFBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xpc3QvbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFsdGgtcmVjb3JkcyB7XHJcbiAgICBoZWlnaHQ6IDQwMHB4O1xyXG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxufVxyXG5cclxuLmhlYWx0aC1yZWNvcmRzLXRhYmxlIHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuXHJcbiAgICB0aCB7XHJcbiAgICAgICAgcGFkZGluZzogMHJlbTtcclxuICAgIH1cclxuXHJcbiAgICB0aGVhZCB7XHJcbiAgICAgICAgdGgge1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZTJlNjtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2RlZTJlNjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICB0Ym9keSB7XHJcbiAgICAgICAgdGQge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG59Il19 */"

/***/ }),

/***/ "./src/app/list/list.component.ts":
/*!****************************************!*\
  !*** ./src/app/list/list.component.ts ***!
  \****************************************/
/*! exports provided: ListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListComponent", function() { return ListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _data_formatter_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../data-formatter.service */ "./src/app/data-formatter.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../database.service */ "./src/app/database.service.ts");




var ListComponent = /** @class */ (function () {
    function ListComponent(_db, _dataFormatter) {
        this._db = _db;
        this._dataFormatter = _dataFormatter;
    }
    ListComponent.prototype.getRecords = function () {
        return this._dataFormatter.applyFilters(this._dataFormatter.filters, this.data);
    };
    ListComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ListComponent.prototype, "data", void 0);
    ListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.component.html */ "./src/app/list/list.component.html"),
            styles: [__webpack_require__(/*! ./list.component.scss */ "./src/app/list/list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_database_service__WEBPACK_IMPORTED_MODULE_3__["DatabaseService"], _data_formatter_service__WEBPACK_IMPORTED_MODULE_1__["DataFormatterService"]])
    ], ListComponent);
    return ListComponent;
}());



/***/ }),

/***/ "./src/app/menu.service.ts":
/*!*********************************!*\
  !*** ./src/app/menu.service.ts ***!
  \*********************************/
/*! exports provided: MenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuService", function() { return MenuService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_menu_structure__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/menu-structure */ "./src/app/shared/menu-structure.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var MenuService = /** @class */ (function () {
    function MenuService(_router) {
        this._router = _router;
        this.structure = _shared_menu_structure__WEBPACK_IMPORTED_MODULE_2__["Structure"];
        this.structure.levels.map(function (level) {
            level.size = "0px";
            return level;
        });
        this.opened = [];
        this.offset = 30;
        this.size = 200;
        this.orientation = 'top';
    }
    MenuService.prototype.isOverlapped = function (id) {
        return this.opened.indexOf(id) !== (this.opened.length - 1);
    };
    MenuService.prototype.toggleOrientation = function () {
        this.orientation === 'top' ?
            this.orientation = 'left' :
            this.orientation = 'top';
    };
    MenuService.prototype.getList = function (id, structure) {
        return structure.lists[id];
    };
    MenuService.prototype.px = function (value) {
        return value + "px";
    };
    MenuService.prototype.action = function (type, path) {
        var self = this;
        var levels = self.structure.levels;
        switch (type) {
            case "toggle":
                if (self.opened.indexOf(path) === -1) {
                    //push one level id to opened if not opened
                    self.opened.push(path);
                }
                else {
                    path === 0 ? this.opened = [] : {};
                }
                adaptSize();
                break;
            case "closeUntil":
                // close all untill level
                closeUntil(path);
                adaptSize();
                break;
            case "link":
                self.opened = [];
                adaptSize();
                this._router.navigateByUrl(path);
                break;
        }
        // helper functions
        function closeUntil(id) {
            // close all levels until id
            console.log(self.opened);
            self.opened = self.opened.slice(0, self.opened.indexOf(id) + 1);
            console.log(self.opened, 'close until', id);
        }
        function adaptSize() {
            var sizes = [];
            // set size of every closed level to 0px;
            levels.map(function (level) {
                !self.opened.includes(level.id) ?
                    level.size = "0px" : {};
                return level;
            });
            // calculate sizes of levels
            sizes = self.opened.map(function (level, index) {
                return self.px(self.size + (index * self.offset));
            }).reverse();
            // assign sizes to levels that are open
            self.opened.map(function (level, index) {
                levels[level].size = sizes[index];
            });
            console.log("adaptSize()", self.opened, sizes);
        }
    };
    MenuService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], MenuService);
    return MenuService;
}());



/***/ }),

/***/ "./src/app/shared/menu-structure.js":
/*!******************************************!*\
  !*** ./src/app/shared/menu-structure.js ***!
  \******************************************/
/*! exports provided: Structure */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Structure", function() { return Structure; });
const Structure = {
 levels: [
   { text: "Home", id: 0, listId: 0 },
   { text: "Music", id: 1, listId: 1 },
   { text: "Software", id: 2, listId: 2 },
   { text: "Apps", id: 3, listId: 3 },
 ],
 lists: [
 {
  id: 0,
  items: [
   { text: "Music", type: "toggle", path: 1 },
   { text: "Software", type: "toggle", path: 2 },
   { text: "Apps", type: "toggle", path: 3 }
  ]
 },
 {
  id: 1,
  items: [
   { text: "Drum and Bass", type: "link", path: "music/drum-and-bass" },
   { text: "Triphop", type: "link", path: "music/triphop" },
   { text: "Psytrance", type: "link", path: "music/psytrance" },
  ]
 },
 {
  id: 2,
  items: [
   { text: "Nucleus", type: "link", path: "software/nucleus" },
   { text: "Dscgen", type: "link", path: "software/dscgen" }
  ]
 },
 {
  id: 3,
  items: [
   { text: "Wasdag", type: "link", path: "apps/wasdag" },
   { text: "Window System", type: "link", path: "apps/window-system" },
   { text: "Basic upload", type: "link", path: "apps/basic-upload" },

  ]
 }]
};

/***/ }),

/***/ "./src/app/toolbar/toolbar.component.html":
/*!************************************************!*\
  !*** ./src/app/toolbar/toolbar.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container toolbar\">\r\n    <form class=\"form-inline\" [formGroup]=\"controlbar\">\r\n        <div class=\"form-group\">\r\n            <div class=\"dropdown\">\r\n                <a class=\"btn btn-secondary btn-sm dropdown-toggle\" href=\"#\" role=\"button\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n                    {{ 'Filter: '+ _dataFormatter.filterType.charAt(0).toUpperCase() + _dataFormatter.filterType.slice(1, _dataFormatter.filterType.length) }}\r\n                </a>\r\n                <div class=\"dropdown-menu\" aria-labelledby=\"filtersDropdown\">\r\n                    <a class=\"dropdown-item\" (click)=\"_dataFormatter.setFilterType('person')\">Person</a>\r\n                    <a class=\"dropdown-item\" (click)=\"_dataFormatter.setFilterType('sleep')\">Sleep</a>\r\n                    <a class=\"dropdown-item\" (click)=\"_dataFormatter.setFilterType('sleep_quality')\">Sleep Quality</a>\r\n                    <a class=\"dropdown-item\" (click)=\"_dataFormatter.setFilterType('activity')\">Activity</a>\r\n                    <a class=\"dropdown-item\" (click)=\"_dataFormatter.setFilterType('physical_condition')\">Physical condition</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <input class=\"form-control\" formControlName=\"terms\" placeholder=\"Filter terms..\"/>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <div class=\"btn btn-primary add-button\" (click)=\"addFilter()\">Add</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <div class=\"range\">\r\n                <mat-form-field>\r\n                    <input matInput [matDatepicker]=\"rangeBegin\" formControlName=\"rangeBegin\" placeholder=\"Begin\">\r\n                    <mat-datepicker-toggle matSuffix [for]=\"rangeBegin\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #rangeBegin></mat-datepicker>\r\n                </mat-form-field>\r\n                </div>\r\n            <div class=\"range\">\r\n                <mat-form-field>\r\n                    <input matInput [matDatepicker]=\"rangeEnd\" formControlName=\"rangeEnd\" placeholder=\"End\">\r\n                    <mat-datepicker-toggle matSuffix [for]=\"rangeEnd\"></mat-datepicker-toggle>\r\n                    <mat-datepicker #rangeEnd></mat-datepicker>\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>\r\n<div class=\"container filters-container card toolbar\">\r\n    <p>Filters:</p>\r\n    <form class=\"form-inline\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <div class=\"form-group\">\r\n                    <button type=\"button\" class=\"btn btn-primary\" *ngFor=\"let filter of getFilters()\" (click)=\"_dataFormatter.removeFilter(filter)\" >{{ filter.prop+\":\"+filter.value  }}</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>"

/***/ }),

/***/ "./src/app/toolbar/toolbar.component.scss":
/*!************************************************!*\
  !*** ./src/app/toolbar/toolbar.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".toolbar {\n  padding: 0px; }\n  .toolbar * {\n    font-size: 12px; }\n  .toolbar .form-group {\n    margin-right: 10px; }\n  .mat-form-field {\n  width: 80px;\n  margin-right: 10px; }\n  .filters-container {\n  padding: 10px;\n  margin-bottom: 10px; }\n  .filters-container .filter {\n    background-color: #efefef;\n    border: 1px solid #ededed;\n    margin-right: 10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdG9vbGJhci9DOlxcVXNlcnNcXGhrc18xXFxEcm9wYm94XFxkZXZlbG9wbWVudFxcaGVhbHRoZGFlbW9uXFxhcHAvc3JjXFxhcHBcXHRvb2xiYXJcXHRvb2xiYXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZLEVBQUE7RUFEaEI7SUFJUSxlQUFlLEVBQUE7RUFKdkI7SUFRUSxrQkFBa0IsRUFBQTtFQUkxQjtFQUNJLFdBQVc7RUFDWCxrQkFBa0IsRUFBQTtFQUd0QjtFQUNJLGFBQWE7RUFDYixtQkFBbUIsRUFBQTtFQUZ2QjtJQUtRLHlCQUF5QjtJQUN6Qix5QkFBeUI7SUFDekIsa0JBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC90b29sYmFyL3Rvb2xiYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudG9vbGJhciB7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICBcclxuICAgICoge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIH1cclxuXHJcbiAgICAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gICAgd2lkdGg6IDgwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5maWx0ZXJzLWNvbnRhaW5lciB7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuXHJcbiAgICAuZmlsdGVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWZlZmVmO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlZGVkZWQ7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/toolbar/toolbar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/toolbar/toolbar.component.ts ***!
  \**********************************************/
/*! exports provided: ToolbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToolbarComponent", function() { return ToolbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _data_formatter_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../data-formatter.service */ "./src/app/data-formatter.service.ts");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../database.service */ "./src/app/database.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);







var ToolbarComponent = /** @class */ (function () {
    function ToolbarComponent(fb, _db, _dataFormatter, router, route) {
        var _this = this;
        this.fb = fb;
        this._db = _db;
        this._dataFormatter = _dataFormatter;
        this.router = router;
        this.route = route;
        this.controlbar = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            terms: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            rangeBegin: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            rangeEnd: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('')
        });
        this.controlbar.get('terms')
            .valueChanges.subscribe(function (value) {
            _this._dataFormatter.setFilterTerms(_this.controlbar.get('terms').value);
        });
        this.controlbar.get('rangeBegin')
            .valueChanges.subscribe(function (value) {
            _this.rangeBegin = value;
            _this.changeRoute();
        });
        this.controlbar.get('rangeEnd')
            .valueChanges.subscribe(function (value) {
            _this.rangeEnd = value;
            _this.changeRoute();
        });
        this._dataFormatter.setFilterType('person');
    }
    ToolbarComponent.prototype.getFilters = function () {
        return this._dataFormatter.getFilters();
    };
    ToolbarComponent.prototype.addFilter = function () {
        this._dataFormatter.addFilter(this._dataFormatter.filter);
        this._dataFormatter.setFilterTerms('');
        this.controlbar.get('terms').reset(); // Also reset here?
    };
    ToolbarComponent.prototype.changeRoute = function () {
        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {
                begin: moment__WEBPACK_IMPORTED_MODULE_6__(this.rangeBegin).format("YYYY-MM-DD"),
                end: moment__WEBPACK_IMPORTED_MODULE_6__(this.rangeEnd).format("YYYY-MM-DD")
            }
        });
    };
    ToolbarComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ToolbarComponent.prototype, "data", void 0);
    ToolbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            selector: 'app-toolbar',
            template: __webpack_require__(/*! ./toolbar.component.html */ "./src/app/toolbar/toolbar.component.html"),
            styles: [__webpack_require__(/*! ./toolbar.component.scss */ "./src/app/toolbar/toolbar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"],
            _data_formatter_service__WEBPACK_IMPORTED_MODULE_1__["DataFormatterService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], ToolbarComponent);
    return ToolbarComponent;
}());



/***/ }),

/***/ "./src/app/views/account-overview/account-overview.component.html":
/*!************************************************************************!*\
  !*** ./src/app/views/account-overview/account-overview.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  account-overview works!\n</p>\n"

/***/ }),

/***/ "./src/app/views/account-overview/account-overview.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/account-overview/account-overview.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2FjY291bnQtb3ZlcnZpZXcvYWNjb3VudC1vdmVydmlldy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/account-overview/account-overview.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/account-overview/account-overview.component.ts ***!
  \**********************************************************************/
/*! exports provided: AccountOverviewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountOverviewComponent", function() { return AccountOverviewComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AccountOverviewComponent = /** @class */ (function () {
    function AccountOverviewComponent() {
    }
    AccountOverviewComponent.prototype.ngOnInit = function () {
    };
    AccountOverviewComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-account-overview',
            template: __webpack_require__(/*! ./account-overview.component.html */ "./src/app/views/account-overview/account-overview.component.html"),
            styles: [__webpack_require__(/*! ./account-overview.component.scss */ "./src/app/views/account-overview/account-overview.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AccountOverviewComponent);
    return AccountOverviewComponent;
}());



/***/ }),

/***/ "./src/app/views/entry/entry.component.html":
/*!**************************************************!*\
  !*** ./src/app/views/entry/entry.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-account></app-account>"

/***/ }),

/***/ "./src/app/views/entry/entry.component.scss":
/*!**************************************************!*\
  !*** ./src/app/views/entry/entry.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2VudHJ5L2VudHJ5LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/entry/entry.component.ts":
/*!************************************************!*\
  !*** ./src/app/views/entry/entry.component.ts ***!
  \************************************************/
/*! exports provided: EntryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntryComponent", function() { return EntryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var EntryComponent = /** @class */ (function () {
    function EntryComponent() {
    }
    EntryComponent.prototype.ngOnInit = function () {
    };
    EntryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-entry',
            template: __webpack_require__(/*! ./entry.component.html */ "./src/app/views/entry/entry.component.html"),
            styles: [__webpack_require__(/*! ./entry.component.scss */ "./src/app/views/entry/entry.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], EntryComponent);
    return EntryComponent;
}());



/***/ }),

/***/ "./src/app/views/health-records/health-records.component.html":
/*!********************************************************************!*\
  !*** ./src/app/views/health-records/health-records.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row no-gutters\">\n  <div class=\"col-md-8\">\n      <app-toolbar [data]=\"getRecords()\"></app-toolbar>\n      <app-list [data]=\"getRecords()\"></app-list>\n  </div>\n  <div class=\"col-md-4\">\n      <app-chart [data]=\"getRecords()\"></app-chart>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/views/health-records/health-records.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/views/health-records/health-records.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2hlYWx0aC1yZWNvcmRzL2hlYWx0aC1yZWNvcmRzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/health-records/health-records.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/views/health-records/health-records.component.ts ***!
  \******************************************************************/
/*! exports provided: HealthRecordsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HealthRecordsComponent", function() { return HealthRecordsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _health_records_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../health-records.service */ "./src/app/health-records.service.ts");
/* harmony import */ var _database_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../database.service */ "./src/app/database.service.ts");
/* harmony import */ var _data_formatter_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../data-formatter.service */ "./src/app/data-formatter.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);








var HealthRecordsComponent = /** @class */ (function () {
    function HealthRecordsComponent(activeRoute, _dataFormatter, _db, _healthRecords, http, router, route) {
        var _this = this;
        this.activeRoute = activeRoute;
        this._dataFormatter = _dataFormatter;
        this._db = _db;
        this._healthRecords = _healthRecords;
        this.http = http;
        this.router = router;
        this.route = route;
        this.records = [];
        this._healthRecords.request({});
        this._healthRecords.$records.subscribe(function (data) {
            _this.records = data;
        });
        this.router.events.subscribe(function (val) {
            var begin = _this.router.routerState.root.firstChild.queryParams['value'].begin;
            var end = _this.router.routerState.root.firstChild.queryParams['value'].end;
            _this._healthRecords.request({ begin: begin, end: end });
            _this._healthRecords.$records.subscribe(function (data) {
                _this.records = data;
                console.log(_this.records);
            });
        });
    }
    HealthRecordsComponent.prototype.getRecords = function () {
        return this.records;
    };
    HealthRecordsComponent.prototype.ngOnInit = function () {
        console.log(this.records);
        if (typeof this.router.routerState.root.firstChild.queryParams['value'].begin == "undefined" &&
            typeof this.router.routerState.root.firstChild.queryParams['value'].end === "undefined") {
            var begin = moment__WEBPACK_IMPORTED_MODULE_7__(Date.now()).subtract(1, 'month').format("YYYY-MM-DD");
            var end = moment__WEBPACK_IMPORTED_MODULE_7__(Date.now()).format("YYYY-MM-DD");
            this.router.navigate([], {
                relativeTo: this.route,
                queryParams: {
                    begin: begin,
                    end: end
                }
            });
        }
    };
    HealthRecordsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
            selector: 'app-health-records',
            template: __webpack_require__(/*! ./health-records.component.html */ "./src/app/views/health-records/health-records.component.html"),
            styles: [__webpack_require__(/*! ./health-records.component.scss */ "./src/app/views/health-records/health-records.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _data_formatter_service__WEBPACK_IMPORTED_MODULE_4__["DataFormatterService"],
            _database_service__WEBPACK_IMPORTED_MODULE_3__["DatabaseService"],
            _health_records_service__WEBPACK_IMPORTED_MODULE_2__["HealthRecordsService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]])
    ], HealthRecordsComponent);
    return HealthRecordsComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\hks_1\Dropbox\development\healthdaemon\app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map