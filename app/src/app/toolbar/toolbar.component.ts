import { DataFormatterService } from './../data-formatter.service';
import { DatabaseService } from './../database.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Input() data: Array<any>;
  public controlbar: FormGroup;
  public keywords;
  public rangeBegin;
  public rangeEnd;

  constructor(public fb: FormBuilder, 
              public _db: DatabaseService, 
              public _dataFormatter: DataFormatterService,
              public router: Router,
              public route: ActivatedRoute) { 

    this.controlbar = new FormGroup({
      terms: new FormControl(''),
      rangeBegin: new FormControl(''),
      rangeEnd: new FormControl('')
    });

    this.controlbar.get('terms')  
      .valueChanges.subscribe(value => {
        this._dataFormatter.setFilterTerms(this.controlbar.get('terms').value);
    });

    this.controlbar.get('rangeBegin')
      .valueChanges.subscribe(value => {
        this.rangeBegin = value;
        this.changeRoute()
    });

    this.controlbar.get('rangeEnd')
      .valueChanges.subscribe(value => {
        this.rangeEnd = value;
        this.changeRoute();
    });

    this._dataFormatter.setFilterType('person');
  }

  getFilters() {
    return this._dataFormatter.getFilters();
  }

  addFilter(){
    this._dataFormatter.addFilter(this._dataFormatter.filter);
    this._dataFormatter.setFilterTerms('');
    this.controlbar.get('terms').reset(); // Also reset here?
  }

  changeRoute() {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { 
          begin:  moment(this.rangeBegin).format("YYYY-MM-DD"), 
          end: moment(this.rangeEnd).format("YYYY-MM-DD") 
        }
      });
  }

  ngOnInit() {
  }

}
