import { AccountService } from './../account.service';
import { MenuService } from '../menu.service';
import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leke-menu',
  templateUrl: './leke-menu.component.html',
  styleUrls: ['./leke-menu.component.scss']
})
export class LekeMenuComponent implements OnInit, AfterViewInit {
  constructor(public _menu: MenuService, private _router: Router, public _account: AccountService, public elm: ElementRef) { 

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    var menu = this.elm.nativeElement.children[0];
    this.elm.nativeElement.parentElement.parentElement.addEventListener('click', event => {
        if(menu.clientWidth < event.clientX && menu.clientWidth !== 0)
          this._menu.action('toggle', 0);
      });
  }
}
