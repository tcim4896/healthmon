import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LekeMenuComponent } from './leke-menu.component';

describe('LekeMenuComponent', () => {
  let component: LekeMenuComponent;
  let fixture: ComponentFixture<LekeMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LekeMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LekeMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
