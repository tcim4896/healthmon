import { AccountService } from './../account.service';
import { MenuService } from '../menu.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public _menu: MenuService, public _account: AccountService) { }
  toggle() {
    this._account.toggle();
  }
  ngOnInit() {
  }

}
