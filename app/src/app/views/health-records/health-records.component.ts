import { HttpClient } from '@angular/common/http';
import { HealthRecordsService } from './../../health-records.service';
import { DatabaseService } from './../../database.service';
import { DataFormatterService } from './../../data-formatter.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-health-records',
  templateUrl: './health-records.component.html',
  styleUrls: ['./health-records.component.scss']
})
export class HealthRecordsComponent implements OnInit {
  public records;

  constructor(private activeRoute: ActivatedRoute, 
              public _dataFormatter: DataFormatterService,
              public _db: DatabaseService,
              public _healthRecords: HealthRecordsService,
              public http: HttpClient,
              private router: Router,
              private route: ActivatedRoute) {
                
    this.records = [];
    this._healthRecords.request({})
    this._healthRecords.$records.subscribe(data => {
      this.records = data;
    })

    this.router.events.subscribe((val) => {
      var begin = this.router.routerState.root.firstChild.queryParams['value'].begin;
      var end = this.router.routerState.root.firstChild.queryParams['value'].end;
      this._healthRecords.request({begin, end});
      this._healthRecords.$records.subscribe(data => {
        this.records = data;
        console.log(this.records);
      })
    });
    
  }
  getRecords(){
   return this.records;   
  }

  ngOnInit() {
    console.log(this.records)
    if(typeof this.router.routerState.root.firstChild.queryParams['value'].begin == "undefined" && 
    typeof this.router.routerState.root.firstChild.queryParams['value'].end === "undefined"){
      var begin = moment(Date.now()).subtract(1, 'month').format("YYYY-MM-DD")
      var end = moment(Date.now()).format("YYYY-MM-DD")
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: { 
          begin, 
          end
        }
      });
    }
  }

}
