import { TestBed } from '@angular/core/testing';

import { HealthRecordsService } from './health-records.service';

describe('HealthRecordsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HealthRecordsService = TestBed.get(HealthRecordsService);
    expect(service).toBeTruthy();
  });
});
