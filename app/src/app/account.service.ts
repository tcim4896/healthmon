import { MenuService } from './menu.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  public display: boolean;

  constructor(public _menu: MenuService) { 
    this.display = false;
  }

  toggle(){
    this._menu.opened.length > 0 ? 
    (()=>{
      this._menu.action('toggle', 0);
      this.display = !this.display
    })() : 
    this.display = !this.display;
  }
}
