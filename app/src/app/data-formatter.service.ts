import { Injectable } from '@angular/core';
import { DatabaseService } from './database.service';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DataFormatterService {
  public filter;
  public filterType;
  public filters;
  public range;
  public filterTerms;
  public records;

  constructor(public _db: DatabaseService,
              public router: Router) {
    this.filter = {};
    this.filterType = "person";
    this.filters = [];
    this.range = {};
    this.filterTerms = '';
  }

  getSleepTimes(a) {
    var beginSleep = a.slice(0, a.indexOf("-")-3);
    var endSleep = a.slice(a.indexOf("-")+1, a.length -2);
    var hoursA = beginSleep.slice(0, beginSleep.indexOf(":"));
    var minA = beginSleep.slice(beginSleep.indexOf(":")+1, beginSleep.length);
    var hoursB = endSleep.slice(0, endSleep.indexOf(":"));
    var minB = endSleep.slice(endSleep.indexOf(":")+1, endSleep.length);

    function minutesToHours(a){ 
      var hours = Math.trunc(a/60); 
      var minutes = a % 60; 
      return parseFloat(hours + "." + minutes); 
    }

    var total = minutesToHours(((12 - parseInt(hoursA) + parseInt(hoursB)) * 60) + parseInt(minA) + parseInt(minB));

    function toFloat(value){
      return parseFloat(value.replace(":", "."));
    }

    beginSleep = toFloat(beginSleep);
    endSleep = toFloat(endSleep);
    
    return {beginSleep, endSleep, total};
  }

  formatDate(date) {
    return moment(date.slice(0, 10)).format("YYYY-MM-DD");
  }

  getColor(factor, sleep) { //
    var color;
    var totalSleep = sleep; // .. temporarily directly put in total sleep time
    switch(factor){
      case 'Total Sleep':
        // console.log(totalSleep)
        if(totalSleep < 6) { // sleep formatting needed to get value
          color = "rgba(225, 0, 0, " + (totalSleep/10) + ")";
        }else{
          color = "rgba(0, 255, 0, " + (totalSleep/10) + ")";
        }
      break;
      default:
        color = "#dee2e6";
    }
    return color;
  }

  getChartData(data){ //
    return [
      {
        text: "Total Sleep",
        values: data.map(d => {
          return this.getSleepTimes(d.sleep).total;
        })
      },
      {
        text: "Begin Sleep",
        values: data.map(d => {
          return this.getSleepTimes(d.sleep).beginSleep;
        })
      },
      {
        text: "End Sleep",
        values: data.map(d => {
          return this.getSleepTimes(d.sleep).endSleep;
        })
      }];
  }

  setFilterType(type){ //
    this.filterType = type;
    this.filter['prop'] = type;
    console.log("[TYPE]:", type)
  }

  setFilterTerms(terms){ //
    this.filterTerms = terms;
    this.filter['value'] = terms;
  }

  addFilter(filter){
    this.filters.push(filter);
    this.filter = {};
    this.filterTerms = ''; //
  }

  removeFilter(filter){
    this.filters = this.filters.filter(f => {
      return f !== filter;
    });
  }

  getFilters(){
    return this.filters;
  }

  applyFilters(filters, records) {
      filters.forEach(filter => {
        records = records.filter(record => record[filter.prop].includes(filter.value));
      });
      //
      console.log(this.filterType, this.filterTerms);
      records = records.filter(record => {
        return record[this.filterType].includes(this.filterTerms)
      })
    return records;
  }

  setSearchQuery(terms) {
    this.filterTerms = terms;
  }

  getRecords(){
    return this.records;
  }


}
