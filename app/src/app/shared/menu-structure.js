export const Structure = {
 levels: [
   { text: "Home", id: 0, listId: 0 },
   { text: "Music", id: 1, listId: 1 },
   { text: "Software", id: 2, listId: 2 },
   { text: "Apps", id: 3, listId: 3 },
 ],
 lists: [
 {
  id: 0,
  items: [
   { text: "Music", type: "toggle", path: 1 },
   { text: "Software", type: "toggle", path: 2 },
   { text: "Apps", type: "toggle", path: 3 }
  ]
 },
 {
  id: 1,
  items: [
   { text: "Drum and Bass", type: "link", path: "music/drum-and-bass" },
   { text: "Triphop", type: "link", path: "music/triphop" },
   { text: "Psytrance", type: "link", path: "music/psytrance" },
  ]
 },
 {
  id: 2,
  items: [
   { text: "Nucleus", type: "link", path: "software/nucleus" },
   { text: "Dscgen", type: "link", path: "software/dscgen" }
  ]
 },
 {
  id: 3,
  items: [
   { text: "Wasdag", type: "link", path: "apps/wasdag" },
   { text: "Window System", type: "link", path: "apps/window-system" },
   { text: "Basic upload", type: "link", path: "apps/basic-upload" },

  ]
 }]
};