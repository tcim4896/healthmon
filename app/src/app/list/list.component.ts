import { DataFormatterService } from './../data-formatter.service';
import { Component, OnInit, Input } from '@angular/core';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() data: Array<any>;
  public range;

  constructor(public _db: DatabaseService, public _dataFormatter: DataFormatterService) {

  }

  getRecords() {
    return this._dataFormatter.applyFilters(
            this._dataFormatter.filters, 
              this.data);
  }

  ngOnInit() {
  }

}
