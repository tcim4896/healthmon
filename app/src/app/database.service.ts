import { DataFormatterService } from './data-formatter.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  constructor(private http: HttpClient) {
  }
   
  getRecords(range){
    var begin = moment(Date.now()).subtract(1, 'months').format("YYYY/MM/DD");
    var end = moment(Date.now()).format("YYYY/MM/DD");

    if(range){
      begin = moment(range.begin).format("YYYY-MM-DD");
      end = moment(range.end).format("YYYY-MM-DD");
    }
    
    const params = new HttpParams()
    .set('begin', begin)
    .set('end', end);

    return this.http.get("http://localhost:3000/health-records", {params});
  }

}
