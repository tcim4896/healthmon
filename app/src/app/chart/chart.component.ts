import { DataFormatterService } from './../data-formatter.service';
import { Component, OnInit, Input } from '@angular/core';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @Input() data: Array<any>;

  constructor(public _db: DatabaseService, 
              public _dataFormatter: DataFormatterService) {
  }

  getData() {
    return this._dataFormatter
      .getChartData(this._dataFormatter.applyFilters(
          this._dataFormatter.filters, 
            this.data
        )
      );
  }
  
  ngOnInit() {
  }

}
