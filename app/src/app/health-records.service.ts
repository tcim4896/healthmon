import { DatabaseService } from './database.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HealthRecordsService {
  public $records;

  constructor(public _db: DatabaseService) { 
    
  }

  request(range){
    this.$records = this._db.getRecords(range);
  }

  get(){
    return this.$records;
  }

}
