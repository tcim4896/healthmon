import { AccountService } from './account.service';
import { DataFormatterService } from './data-formatter.service';
import { DatabaseService } from './database.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ChartComponent } from './chart/chart.component';
import { HealthRecordsComponent } from './views/health-records/health-records.component';
import { AccountComponent } from './account/account.component';
import { EntryComponent } from './views/entry/entry.component';
import { HeaderComponent } from './header/header.component';
import { LekeMenuComponent } from './leke-menu/leke-menu.component';
import { MenuService } from './menu.service';
import { AccountOverviewComponent } from './views/account-overview/account-overview.component';


const routes: Routes = [
  { path: '', redirectTo: '/healthrecords', pathMatch: 'full' },
  { path: 'entry', component: EntryComponent },
  { path: 'healthrecords', component: HealthRecordsComponent, runGuardsAndResolvers: 'always'},
  { path: 'healthrecords/:range', component: HealthRecordsComponent, runGuardsAndResolvers: 'always'},
];

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ToolbarComponent,
    ChartComponent,
    HealthRecordsComponent,
    AccountComponent,
    EntryComponent,
    HeaderComponent,
    LekeMenuComponent,
    AccountOverviewComponent
  ],
  imports: [
    BrowserModule,  
    RouterModule.forRoot(routes, {enableTracing: false}),
    HttpClientModule,
    MatInputModule,
    MatDatepickerModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [DataFormatterService, DatabaseService, MenuService, AccountService, MatDatepickerModule, Document],
  bootstrap: [AppComponent]
})
export class AppModule { }
