import pymongo
from pymongo import MongoClient
from datetime import datetime

client = MongoClient()
client = MongoClient('localhost', 27017)
db = client.pyhealthapp

health_records = db.health_records

# // pseudo //
# get_user_data 
# f: prompt user for data
# 
# create_health_record
# f: create record in db

health_record = {}

def get_user_data():
    health_record['date'] = datetime.utcnow()
    health_record['person'] = input("What is your name? ")
    health_record['sleep_quality'] = input("How did you sleep(Good, Bad, Mwa)? ")
    health_record['sleep'] = input("How long did you sleep?(e.g 8:00PM - 9:00AM)? ")
    health_record['activity'] = input("Did you do some activities? ")
    health_record['medication'] = input("Did you take medication? ")


def createHealthRecord():
    health_records.insert_many([health_record])

# tcim_health_records = health_records.find({})

# fn: list items
print(health_records)
for document in health_records:
          print(document)

# fn: removing records
# health_records.delete_many({})

# get_user_data()
# createHealthRecord()

print('Thank you..')